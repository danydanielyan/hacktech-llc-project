from flask import Flask

app = Flask(__name__)


def return_hello():
    return "Hello, World!"


@app.route("/")
def hello():
    return return_hello()


app.run(host='0.0.0.0', port=5000)
