FROM python:3.10
COPY . .
RUN pip install flask pytest
CMD ["python", "hello.py"]